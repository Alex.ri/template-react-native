import { StyleSheet, View, Image } from 'react-native';
import { FadeInView } from '../../shared/animations/Animations'

export function SplashScreen() {
  return (
    <View style={styles.container}>
      <FadeInView>
        <Image style={styles.logo} source={require('../../assets/img/logo/logo.png')} />
      </FadeInView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 250,
    height: 250,
    resizeMode: "contain",
  }
});

export default SplashScreen;
