import { StyleSheet, Text, View } from 'react-native';

export function Account() {
  return (
    <View style={styles.container}>
      <Text>Account work !</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Account;
