import 'react-native-gesture-handler';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { StyleSheet } from 'react-native';
import Home from './components/Home';
import Account from './components/Account';
import { NavigationContainer } from '@react-navigation/native';
import { useState } from 'react';
import SplashScreen from './components/common/SplashScreen';
import Login from './components/auth/Login';

const Drawer = createDrawerNavigator();

export default function App() {
  const [isSignedIn, setIsSignedIn] = useState(true);
  const [appReady, setAppReady] = useState(false);

  if (!appReady) {
    setTimeout(() => {
      setAppReady(true);
    }, 3000);
    return <SplashScreen></SplashScreen>;
  }

  if (!isSignedIn && appReady) {
    return <Login></Login>
  }

  if (isSignedIn && appReady) {
    return (
    <NavigationContainer>
      <Drawer.Navigator useLegacyImplementation>
        <Drawer.Screen name="Home" component={Home} />
        <Drawer.Screen name="Account" component={Account} />
      </Drawer.Navigator>
    </NavigationContainer>
    )
  }
}

